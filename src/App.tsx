import React from 'react';
import './App.css';
import PetList from './components/PetList';

const App: React.FC = () => {
  return (
    <div className="App">
      <PetList />
    </div>
  );
}

export default App;
