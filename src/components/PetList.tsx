import React, { useState, useEffect } from 'react';

type statusType = "available" | "pending" | "sold"
interface Pet {
    id: number,
    name: string,
    category: {
        id: number,
        name: string
    },
    status: statusType
}

const PetList: React.FC = () => {

    const [status, setStatus] = useState<statusType>("available");
    const [pets, setPets] = useState<Pet[]>([]);

    useEffect(() => {
        fetch(`https://petstore.swagger.io/v2/pet/findByStatus?status=${status}`).then((response) => {
            response.json().then(function (resPets) {
                setPets(resPets.slice(0, 10));
            });
        });

    }, [status]);

    return (
        <div>
            <select value={status} onChange={(e) => setStatus(e.target.value as statusType)}>
                <option>available</option>
                <option>pending</option>
                <option>sold</option>
            </select>
            <table>
                <thead>
                    <th>
                        ID
                </th>
                    <th>
                        name
                </th>
                    <th>
                        status
                </th>
                </thead>
                <tbody>
                    {pets.map(pet => <tr><td>{pet.id}</td><td>{pet.name}</td><td>{pet.status}</td></tr>)}
                </tbody>
            </table>
        </div>
    );
}

export default PetList;
